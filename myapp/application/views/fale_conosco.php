<div id="homebody">
    <div class="alinhado-centro borda-base espaco-vertical">
        <h3>Fale conosco</h3>        
    </div>
  <div class="row-fluid">
    <?php
    echo validation_errors();            
    echo form_open(base_url('cadastro/enviar_email_confirmacao'),array('id'=>'form_cadastro')) .
    "<div class='span4'>" .
    	form_input(array('id'=>'nome','name'=>'nome','Placeholder'=>'Nome','value'=>set_value('nome'))) .    
        form_input(array('id'=>'email','name'=>'email','value'=>'','Placeholder'=>'E-mail','value'=>set_value('email'))) .
        form_textarea(array('name'=>'txt_descricao','id'=>'txt_descricao', 'Placeholder'=>'Mensagem')) .   	
    	form_submit('btn_cadastrar','Enviar Mensagem') .
    "</div>" .
    form_close();
    ?>
  </div>  
</div>
