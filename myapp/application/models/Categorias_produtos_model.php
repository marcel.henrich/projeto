<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*******************************************************************************
* Model das categorias.
*******************************************************************************/
class Categorias_produtos_model extends CI_Model {


    public function __construct(){
        parent::__construct();
    }

	
	public function adicionar($produto,$categoria){
		$dados['produto'] = $produto;
		$dados['categoria'] = $categoria;
		return $this->db->insert('produtos_categorias',$dados);
	}
	

}